#!/bin/sh

# If a command fails then the deploy stops
set -e

printf "\033[0;32mDeploying updates to Bitbucket...\033[0m\n"

# Build the project.
lektor build --output-path ../masau-1128.bitbucket.io

# Go To Public folder
cd ../masau-1128.bitbucket.io

# Add changes to git.
git add .

# Commit changes.
msg="Build site on $(date)"
if [ -n "$*" ]; then
	msg="$*"
fi
git commit -m "$msg"

# Push source and build repos.
git push origin master
